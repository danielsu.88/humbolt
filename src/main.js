/* eslint-disable */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import DatePicker from 'vue2-datepicker'

import VuePhoneNumberInput from 'vue-phone-number-input';

Vue.config.productionTip = false
    // Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue2-datepicker/index.css'
import 'vue-phone-number-input/dist/vue-phone-number-input.css';

Vue.use(VuePhoneNumberInput)

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)

// Optionally install the BootstrapVue icon components plugin
Vue.use(DatePicker)

Vue.use(IconsPlugin)


Vue.use(VueAxios, axios)

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')